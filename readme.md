### Installation:

- `composer install`
- `npm install`
- Create .env file (example below)
- `php artisan key:generate`
- Change the credentials on your .env file before you start migrating. Make sure you have a database set ready.
- `php artisan migrate`
   * Save the features table somewhere else first and migrate the 'services' table first incase you have a problem with the above 'php artisan migrate'.

- `php artisan db:seed -- Seed all the table by making sure they are not commented on the DatabaseSeeder.php.
- Install gulp (sudo npm install -g gulp)
- run `gulp` or `gulp watch`
- Configure `app.php` and `mail.php`

### Example .env file:

    APP_ENV=local
    APP_DEBUG=true
    APP_URL=http://localhost
    APP_KEY=WILL BE GENERATED
    
    DB_HOST=localhost
    DB_DATABASE=laravel
    DB_USERNAME=root
    DB_PASSWORD=root
    
    CACHE_DRIVER=file
    SESSION_DRIVER=file
    QUEUE_DRIVER=sync
    
    MAIL_DRIVER=smtp
    MAIL_HOST=mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    
    GITHUB_CLIENT_ID=
    GITHUB_CLIENT_SECRET=
    GITHUB_REDIRECT=
    
    FACEBOOK_CLIENT_ID=
    FACEBOOK_CLIENT_SECRET=
    FACEBOOK_REDIRECT=
    
    TWITTER_CLIENT_ID
    TWITTER_CLIENT_SECRET
    TWITTER_REDIRECT=
    
    GOOGLE_CLIENT_ID=
    GOOGLE_CLIENT_SECRET=
    GOOGLE_REDIRECT=


## Socialite

To configure socialite, add your credentials to your .env file. The redirects must follow the convention ```http://mysite.com/auth/login/SERVICE```. Available services are ```github```, ```facebook```, ```twitter```, and ```google```. Links to each are included in ```login.blade.php```.

If you are getting a ```cURL error 60``` on localhost, follow [these directions](http://stackoverflow.com/questions/28635295/laravel-socialite-testing-on-localhost-ssl-certificate-issue).
    
## Troubleshooting

If for any reason something goes wrong, try each of the following:

Delete the `composer.lock` file

Run the `dumpautoload` command

       $ composer dumpautoload -o
       
If the above fails to fix, and the command line is referencing errors in `compiled.php`, do the following:
       
Delete the `storage/framework/compiled.php` file

Credentials

Username:  admin@admin.com
Password:  admin

Username:  user@user.com
Password:  user