<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model {

	protected $table = 'prices';
	public $timestamps = true;
	protected $fillable = array('title', 'description');
	protected $visible = array('id','title', 'description');

	public function services()
	{
		return $this->hasMany('Service');
	}
}