<?php

/*
 * Global helpers file with misc functions
 */

if (! function_exists('app_name')) {
	/**
	 * Helper to grab the application name
	 *
	 * @return mixed
	 */
	function app_name() {
		return \Config::get('app.name');
	}

	function myCustomHelper(){
	    return
	        "Im a custom helper";
	}

	function getCategories(){
		$categories = array();
		foreach (App\Category::all() as $category) {
			$categories[$category->title] = $category->title;
			// $category[array_push($category, array($category->name => $category->name))];
		}
		return $categories;
	}

	function getSuppliers(){
		$suppliers = array();
		foreach (App\Supplier::all() as $supplier) {
			$suppliers[$supplier->title] = $supplier->title;
			// array_push($counties, array($supplier->name => $supplier->name));
		}
		return $suppliers;
	}

	function getFeatures(){
		// $features = array();
		$features = App\Feature::all();
		$features = $features->services->toArray();
		foreach ($features as $feature) {
			$features[$feature->description] = $feature->description;
			// array_push($counties, array($feature->name => $feature->name));
		}
		return $features;
	}
}