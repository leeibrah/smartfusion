<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {

	protected $table = 'suppliers';
	public $timestamps = true;
	protected $fillable = array('title', 'username', 'key', 'url');
	protected $visible = array('id','title', 'username', 'key', 'url');

}