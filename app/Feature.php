<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model {

	protected $table = 'features';
	// public $timestamps = true;
	protected $fillable = array('description');
	protected $visible = array('id','description');


	public function services(){

		return $this->belongsTomany('App\Service');
	}
}