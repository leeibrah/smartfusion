<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

	protected $table = 'status';
	public $timestamps = true;
	protected $guarded = array('title');
	protected $hidden = array('title');

}