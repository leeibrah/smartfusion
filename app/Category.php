<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'categories';
	public $timestamps = true;
	protected $fillable = array('title');
	protected $visible = array('id','title');

	public function services()
	{
		return $this->hasMany('Service');
	}

}