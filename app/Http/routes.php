<?php


//Socialite Integration
Route::get('auth/login/{provider}', ['as' => 'auth.provider', 'uses' => 'Auth\AuthController@loginThirdParty']);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
Route::group([
    'middleware' => 'vault.routeNeedsRole',
    'role' => ['Administrator'],
    'redirect' => '/',
    'with' => ['error', 'You do not have access to this section.']
], function()
{
    Route::group(['prefix' => 'admin'], 
    {

        #Services
        Route::get('service/data', 'ServiceController@data');
        Route::resource('service', 'ServiceController');
        Route::get('service/1/edit', 'ServiceController');

        #Service Activation
        Route::get('service/activate/{id}', 'ServiceController@activate');
        Route::get('service/deactivate', 'ServiceController@deactivate');

        #Categories
        Route::get('category/data', 'CategoryController@data');
        Route::resource('category', 'CategoryController');

        #Features
        Route::get('feature/data', 'FeatureController@data');
        Route::resource('feature', 'FeatureController');

        #Prices
        Route::get('price/data', 'PriceController@data');
        Route::resource('price', 'PriceController');

        #Statuses
        Route::get('status/data', 'StatusController@data');
        Route::resource('status', 'StatusController');

        #Suppliers
        Route::get('supplier/data', 'SupplierController@data');
        Route::resource('supplier', 'SupplierController');
        Route::get('supplier/check', 'SupplierController@check');

        #Orders
        Route::get('order', 'OrderController@index');
        Route::get('order/data', 'OrderController@data');
        Route::get('order/edit', 'OrderController@edit');
    });
});

Route::group([
    'middleware' => 'vault.routeNeedsRole',
    'role' => ['User'],
    'redirect' => '/',
    'with' => ['error', 'You do not have access to this section.']
], function()
{
    Route::group(['prefix' => 'client'], function ()
    {
        Route::get('order/create', 'OrderController@create');

    });
});


Route::get('/', 'FrontendController@index');

Route::get('dashboard', 'DashboardController@index');



Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

