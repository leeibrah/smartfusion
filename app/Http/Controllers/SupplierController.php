<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\SupplierRequest;
use App\Http\Controllers\Controller;
use App\Supplier;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class SupplierController extends Controller {


  public function index()
  {
    return view('backend.suppliers.index');
  }


  public function create()
  {
      return view('backend.suppliers.add');
  }


  public function store(SupplierRequest $request)
  {
      Supplier::create($request->all());
      return redirect('admin/supplier');
  }


  public function show($id)
  {
    
  }


  public function edit($id)
  {
      $supplier = Supplier::findOrFail($id);
      return view('backend.suppliers.edit' ,compact('supplier'));
  }


  public function update($id, SupplierRequest $request)
  {
      $supplier = Supplier::findOrFail($id);
      $supplier->update($request->all());
      return redirect('admin/supplier');
  }


  public function destroy($id)
  {
    
  }
  public function data()
    {
         $suppliers = Supplier::select(['id','title','url']);
      
    return Datatables::of($suppliers)
        
        ->addColumn('action', '
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/supplier/\' . $id . \'/check\' ) }}}" class="btn btn-success btn-xs" ><i class="fa fa-pencil"></i> Check</a>
                <a href="{{{ URL::to(\'admin/supplier/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                <a href="{{{ URL::to(\'admin/supplier/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')
        
        ->make(true);
    }

    public function check($id)
    {

    }
}

?>