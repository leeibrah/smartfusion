<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Order;
use Carbon\Carbon;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class OrderController extends Controller {


  public function index()
  {
      return view('backend.orders.index');
  }


  public function create()
  {
      return view('backend.orders.add');
  }


  public function store(OrderRequest $request)
  {
      Order::create($request->all());
      return redirect('admin/order');
  }


  public function show($id)
  {
    
  }


  public function edit($id)
  {
      $order = Order::findOrFail($id);
      return view('backend.orders.edit' ,compact('order'));
  }


  public function update($id, OrderRequest $request)
  {
      $order = Order::findOrFail($id);
      $order->update($request->all());
      return redirect('admin/order');
  }


  public function destroy($id)
  {
    
  }
    public function data(Request $request)
    {
        $orders = DB::table('orders')->select(['id','imei','service','status','created_at','updated_at']);

        return Datatables::of($orders)

            ->filter(function ($query) use ($request) {
                if ($request->has('imei')) {
                    $query->where('imei', 'like', "%{$request->get('imei')}%");
                }
                if ($request->has('service')) {
                    $query->where('service', 'like', "%{$request->get('service')}%");
                }
            })
            ->editColumn('created_at', function ($order) {
                return $order->created_at ? with(new Carbon($order->created_at))->format('d/m/Y') : '';
            })
            ->editColumn('updated_at', function ($order) {
                return $order->updated_at ? with(new Carbon($order->updated_at))->format('d/m/Y') : '';
            })
            ->addColumn('action', '
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/service/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
            </div>
            ')
            ->make(true);
    }
}

?>