<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ServiceRequest;
use App\Http\Controllers\Controller;
use App\Service;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;
use Illuminate\Support\Facades\Auth;

use App\Feature;
use Input;

use Carbon\Carbon;

class ServiceController extends Controller {


  public function index()
  {

    $services = \DB::table('services')->get();    
    $price = \DB::table('prices')->where('id',1)->pluck('title');

    return view('backend.services.index')
    ->with('services', $services)
    ->with('price', $price);
  }


  public function create()
  {

      // $uri = Request::url("http://localhost:9900/admin/service/create");
      // $uri = Request::is('service/create');
      // dd($uri);

      $categories = \DB::table('categories')->lists('title', 'id');
      $features = \DB::table('features')->lists('description', 'id');
      $suppliers = \DB::table('suppliers')->lists('title', 'id');
      $prices = \DB::table('prices')->lists('title', 'id');
      $selectedCategoryKey = null;
      $selectedSupplierKey = null;
      $selectedPriceKey = null;
      // $company_lists = RecordCompany::lists('company_name', 'id');


      return view('backend.services.add')
          ->with('categories', $categories)
          ->with('features', $features)
          ->with('prices', $prices)
          ->with('suppliers', $suppliers)
          ->with('selectedSupplierKey', $selectedSupplierKey)
          ->with('selectedCategoryKey', $selectedCategoryKey)
          ->with('selectedPriceKey', $selectedPriceKey);
  }


  public function store(ServiceRequest $request)
  {          

      $service = Auth::user()->services()->create($request->all());      

      $service->features()->attach($request->input('feature_list'));
 
      return redirect('admin/service');
  }

  public function activate($id){

      // $service = Service::findOrFail($id);
      dd('service');

      // return redirect('admin/service');
  }

  public function show($id)
  {
      $service = Service::findOrFail($id);

      return view('backend.services.show', compact('service'));
  }


  public function edit($id)
  {
      $service = Service::findOrFail($id);

      $categories = \DB::table('categories')->lists('title', 'id');
      $features = \DB::table('features')->lists('description', 'id');
      $suppliers = \DB::table('suppliers')->lists('title', 'id');
      $prices = \DB::table('prices')->lists('title', 'id');
      $selectedCategoryKey = \DB::table('services')->where('id', $id)->pluck('category');
      $selectedSupplierKey = \DB::table('services')->where('id', $id)->pluck('supplier');
      $selectedPriceKey = \DB::table('services')->where('id', $id)->pluck('price');

      // dd($categories);
      return view('backend.services.edit')
        ->with('service', $service)
        ->with('categories', $categories)
        ->with('suppliers', $suppliers)
        ->with('features', $features)
        ->with('prices', $prices)
        ->with('selectedCategoryKey', $selectedCategoryKey)
        ->with('selectedPriceKey', $selectedPriceKey)
        ->with('selectedSupplierKey', $selectedSupplierKey);
  }


  public function update($id, ServiceRequest $request)
  {
      // dd($request->all());

      $service = Service::findOrFail($id);
      $service->update($request->all());

      $service->features()->sync($request->input('feature_list'));

      return redirect('admin/service');
  }

  

  public function destroy($id)
  {
    
  }

  public function data()
    {
         $services = Service::select(['id','category','title','price', 'description']);

    return Datatables::of($services)
        
        ->addColumn('action',  '
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/service/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                <a href="{{{ URL::to(\'admin/service/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')
        
        ->make(true);
    }

  public function see(){

    var_dump('Sen');
  }
}

