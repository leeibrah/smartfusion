<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\PriceRequest;
use App\Http\Controllers\Controller;
use App\Price;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class PriceController extends Controller {


    public function index()
    {
        return view('backend.services.prices.index');
    }


    public function create()
    {
        return view('backend.services.prices.add');
    }


    public function store(PriceRequest $request)
    {
        Price::create($request->all());
        return redirect('admin/price');
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $price = Price::findOrFail($id);
        return view('backend.services.prices.edit' ,compact('price'));
    }


    public function update($id, PriceRequest $request)
    {
        $price = Price::findOrFail($id);
        $price->update($request->all());
        return redirect('admin/price');
    }



    public function destroy($id)
    {

    }
    public function data()
    {
        $prices = Price::select(['id','title','description']);

        return Datatables::of($prices)

            ->addColumn('action','
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/price/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                <a href="{{{ URL::to(\'admin/price/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')

            ->make(true);
    }
}