<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\StatusRequest;
use App\Http\Controllers\Controller;
use App\Status;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class StatusController extends Controller {


    public function index()
    {
        return view('backend.status.index');
    }


    public function create()
    {
        return view('backend.status.add');
    }


    public function store(StatusRequest $request)
    {
        Status::create($request->all());
        return redirect('admin/status');
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        $status = Status::findOrFail($id);
        return view('backend.status.edit' ,compact('status'));
    }


    public function update($id, StatusRequest $request)
    {
        $status = Status::findOrFail($id);
        $status->update($request->all());
        return redirect('admin/status');
    }



    public function destroy($id)
    {

    }
    public function data()
    {
        $statuses = Status::select(['title']);

        return Datatables::of($statuses)

            ->addColumn('action','
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/status/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                            </div>
            ')

            ->make(true);
    }
}