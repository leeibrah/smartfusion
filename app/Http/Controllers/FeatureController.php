<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\FeatureRequest;
use App\Http\Controllers\Controller;
use App\Feature;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class FeatureController extends Controller {


  public function index()
  {
    return view('backend.services.features.index');
  }


  public function create()
  {
      return view('backend.services.features.add');
  }


  public function store(FeatureRequest $request)
  {
      Feature::create($request->all());
      // dd($request->all());
      return redirect('admin/feature');
  }


  public function show($id)
  {
    
  }


  public function edit($id)
  {
      $feature = Feature::findOrFail($id);
      return view('backend.services.features.edit' ,compact('feature'));
  }


  public function update($id, FeatureRequest $request)
   {
        $feature = Feature::findOrFail($id);
        $feature->update($request->all());
        return redirect('admin/feature');
   }


  public function destroy($id)
  {
    $feature = Feature::findOrFail($id);
    $feature->delete();
 
    return Redirect::route('features.index')->with('message', 'features deleted.');
  }


  public function data()
    {
         $features = Feature::select(['id','description']);
      
    return Datatables::of($features)
        
        ->addColumn('action', '
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/feature/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                <a href="{{{ URL::to(\'admin/feature/\' . $id ) }}}" class="btn btn-xs btn-danger iframe"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')
        
        ->make(true);
    }
}

?>