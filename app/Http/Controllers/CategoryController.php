<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;
use App\Category;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;

class CategoryController extends Controller {


  public function index()
  {
    return view('backend.services.categories.index');
  }


  public function create()
  {
      return view('backend.services.categories.add');
  }


  public function store(CategoryRequest $request)
  {
      Category::create($request->all());
      return redirect('admin/category');
  }


  public function show($id)
  {
    
  }


  public function edit($id)
  {
      $category = Category::findOrFail($id);
      return view('backend.services.categories.edit' ,compact('category'));
  }


  public function update($id, CategoryRequest $request)
  {
      $category = Category::findOrFail($id);
      $category->update($request->all());
      return redirect('admin/category');
  }



  public function destroy($id)
  {
    
  }
  public function data()
    {
         $categories = Category::select(['id','title']);
      
    return Datatables::of($categories)
        
        ->addColumn('action','
             <div class="btn-group">
                <a href="{{{ URL::to(\'admin/category/\' . $id . \'/edit\' ) }}}" class="btn btn-primary btn-xs iframe" ><i class="fa fa-pencil"></i> Edit</a>
                <a href="{{{ URL::to(\'admin/category/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            ')
        
        ->make(true);
    }
}

?>