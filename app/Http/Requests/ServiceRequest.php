<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class ServiceRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'description'=> 'required',
            'category'=> 'required',
            'price'=> 'required'

        ];
    }

    public function authorize()
    {
        // Only allow logged in users
        // return \Auth::check();
        // Allows all users in
        return true;
    }
}