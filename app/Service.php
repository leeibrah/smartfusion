<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model {

	protected $table = 'services';
	public $timestamps = false;
	protected $fillable = array('title', 'category', 'description', 'image', 'delivery_time', 'price', 'supplier', 'user_id');
	protected $visible = array('id','title', 'category', 'description', 'image', 'delivery_time', 'price', 'supplier');
	protected $dates = ['delivery_time'];

	// public function features()
	// {
	// 	return $this->hasMany('Feature', 'id');
	// }

	public function supplier()
	{
		return $this->hasOne('Supplier', 'id');
	}

	public function category()
	{
		return $this->belongsTo('Category', 'id');
	}

	public function price()
	{
		return $this->belongsTo('App\Price');
	}

	public function scopePublished($query){

		$query->where('delivery_time', '<=', Carbon::now()->toDateTimeString());
	}

	public function setPublishedAttribute($date){

		$this->attributes['delivery_time'] = Carbon::parse($date);
	}


	public function user(){

		return $this->belongsTo('App\User');
	}


	public function features(){

		return $this->belongsToMany('App\Feature')->withTimestamps();
	}

	//Get the list of the features associated with given service. 
	//Mostly used on the edits

	//Returns an array
	public function getFeatureListAttribute(){

		return $this->features->lists('id');
	}

	public function getCategoryListAttribute(){

		return $this->features->lists('id');
	}

}