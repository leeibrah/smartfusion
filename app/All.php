<?php

class All extends Eloquent {

	public static function getCategories(){
		$categories = array();
		foreach (Category::all() as $category) {
			$categories[$category->title] = $category->name;
			// array_push($counties, array($category->name => $category->name));
		}
		return $categories;
	}


	public static function getSuppliers(){
		$suppliers = array();
		foreach (Supplier::all() as $supplier) {
			$suppliers[$supplier->title] = $supplier->name;
			// array_push($counties, array($supplier->name => $supplier->name));
		}
		return $suppliers;
	}
}