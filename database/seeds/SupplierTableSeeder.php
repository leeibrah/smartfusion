<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Supplier;

class SupplierTableSeeder extends Seeder
{
    public function run()  
    {

        $faker = Faker\Factory::create();

        Supplier::truncate();

        foreach(range(1,5) as $index)  
        {  
            Supplier::create([                 
                'title' => $faker->sentence(1), 
                'username' => $faker->sentence(1),
                'key' => $faker->sentence(1),
                'url' => $faker->sentence(1)
            ]);  
        }  
    }  
}
