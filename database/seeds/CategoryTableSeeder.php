<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    // public function run()
    // {
    //     TestDummy::times(5)->create('App\Post');
    // }
    public function run()  
    {

        $faker = Faker\Factory::create();

        Category::truncate();

        foreach(range(1,5) as $index)  
        {  
            Category::create([                 
                'title' => $faker->sentence(1)  
            ]);  
        }  
    }  
}
