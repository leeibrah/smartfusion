<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;
use App\Feature;

class FeatureTableSeeder extends Seeder
{
    public function run()  
    {

        $faker = Faker\Factory::create();

        Feature::truncate();

        foreach(range(1,5) as $index)  
        {  
            Feature::create([                 
                
                'description' => $faker->sentence(1)
            ]);  
        }  
    }  
}
