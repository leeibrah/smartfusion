<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration {

	public function up()
	{
		Schema::create('services', function(Blueprint $table) {
			$table->increments('id');
			$table->text('title');
			$table->integer('category');
			$table->text('description');
			$table->string('image');
			$table->timestamp('delivery_time');
			$table->string('price');
			$table->string('price_b')->nullable();
			$table->string('price_c')->nullable();
			$table->string('price_d')->nullable();		
			$table->integer('supplier')->nullable();
			$table->boolean('active');
			$table->integer('user_id')->unsigned();
			$table->timestamps();

			$table->foreign('user_id')
				  ->references('id')
				  ->on('users')
				  ->onDelete('cascade');
		});

	}

	public function down()
	{
		Schema::drop('services');
	}
}