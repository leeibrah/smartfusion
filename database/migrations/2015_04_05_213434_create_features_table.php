<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('features', function(Blueprint $table) {
			$table->increments('id');
			$table->string('description');	
			$table->timestamps();
		});

		Schema::create('feature_service', function(Blueprint $table){

			$table->integer('feature_id')->unsigned()->index();
			$table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');

			$table->integer('service_id')->unsigned()->index();
			$table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

			$table->timestamps();

		});
	}

	public function down()
	{
		Schema::drop('features');
		Schema::drop('feature_service');
		// Schema::drop('features');
	}
}