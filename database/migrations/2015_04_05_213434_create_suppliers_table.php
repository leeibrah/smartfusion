<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table) {
			$table->increments('id');
			$table->text('title');
			$table->string('username');
			$table->string('key');
			$table->string('url');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('suppliers');
	}
}