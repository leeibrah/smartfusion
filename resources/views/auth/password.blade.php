@extends('layouts.app')

@section('content')
    <body class="pace-top bg-white">
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin register -->
        <div class="register register-with-news-feed">
            <!-- begin news-feed -->
            <div class="news-feed">
                <div class="news-image">
                    <img src="{{ asset('assets/img/login-bg/bg-5.jpg') }}" alt="" />
                </div>
                <div class="news-caption">
                    <h4 class="caption-title"><i class="fa fa-edit text-primary"></i> Announcing the Color Admin app</h4>
                    <p>
                        As a Color Admin Apps administrator, you use the Color Admin console to manage your organization’s account, such as add new users, manage security settings, and turn on the services you want your team to access.
                    </p>
                </div>
            </div>
            <!-- end news-feed -->
            <!-- begin right-content -->
            <div class="right-content">

                <!-- begin register-header -->
                <h1 class="register-header">
                    Reset Password
                    <small>Create your Color Admin Account. It’s free and always will be.</small>
                </h1>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                <!-- end register-header -->
                <!-- begin register-content -->
                <div class="register-content">
                    {!! Form::open(['to' => 'password/email', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                    <div class="row m-b-15">
                        {!! Form::input('email', 'email', old('email'), ['class' => 'form-control input-lg', 'placeholder'=>'E-mail Address']) !!}
                    </div>

                    <div class="register-buttons">
                        {!! Form::submit('Send Password Reset Link', ['class' => 'btn btn-primary btn-block btn-lg']) !!}
                    </div>
                    {!! Form::close() !!}

                    <hr />
                    <p class="text-center text-inverse">
                        &copy; {{app_name()}} All Right Reserved 2015
                    </p>

                </div>
                <!-- end register-content -->
            </div>
            <!-- end right-content -->
        </div>
        <!-- end register -->
    </div>
    </body>
@endsection