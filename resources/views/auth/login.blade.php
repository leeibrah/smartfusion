@extends('layouts.app')

@section('content')

    <body class="pace-top bg-white">
    <!-- begin #page-loader -->
    <div id="page-loader" class="fade in"><span class="spinner"></span></div>
    <!-- end #page-loader -->

    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin login -->
        <div class="login login-with-news-feed">
            <!-- begin news-feed -->
            <div class="news-feed">
                <div class="news-image">
                    <img src="{{ asset('assets/img/login-bg/bg-7.jpg') }}" data-id="login-cover-image" alt="" />
                </div>
                <div class="news-caption">
                    <h4 class="caption-title"><i class="fa fa-diamond text-primary"></i> Announcing the Color Admin app</h4>
                    <p>
                        Download the Color Admin app for iPhone®, iPad®, and Android™. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </p>
                </div>
            </div>
            <!-- end news-feed -->
            <!-- begin right-content -->
            <div class="right-content">
                <!-- begin login-header -->
                <div class="login-header">
                    <div class="brand">
                        <span class="logo"></span> Color Admin
                        <small>responsive bootstrap 3 admin template</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sign-in"></i>
                    </div>
                </div>
                <!-- end login-header -->
                <!-- begin login-content -->
                <div class="login-content">
                    {!! Form::open(['url' => 'auth/login', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                        <div class="form-group m-b-15">
                                {!! Form::input('email', 'email', old('email'), ['class' => 'form-control input-lg', 'placeholder'=>'E-mail Address']) !!}
                        </div>
                        <div class="form-group m-b-15">
                            {!! Form::input('password', 'password', null, ['class' => 'form-control input-lg', 'placeholder'=>'Password']) !!}
                        </div>
                        <div class="checkbox m-b-30">
                            <label>
                                {!! Form::checkbox('remember') !!} Remember Me
                            </label>
                        </div>
                        <div class="login-buttons">
                            {!! Form::submit('Login', ['class' => 'btn btn-primary btn-block btn-lg', 'style' => 'margin-bottom:15px']) !!}
                            <div class="row text-center">
                            {!! link_to('password/email', 'Forgot Your Password?') !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                    <hr />
                    <div class="row text-center">
                        {!! link_to_route('auth.provider', 'Login with Facebook', 'facebook') !!}&nbsp;|&nbsp;
                        {!! link_to_route('auth.provider', 'Login with Twitter', 'twitter') !!}&nbsp;|&nbsp;
                        {!! link_to_route('auth.provider', 'Login with Google', 'google') !!}
                    </div>
                    <hr />
                        <div class="m-t-20 m-b-20 p-b-10">
                            Not a member yet? Click {!! link_to('auth/register', 'here') !!} to register.
                        </div>
                        <hr />
                        <p class="text-center text-inverse">
                            &copy; {{app_name()}} All Right Reserved 2015
                        </p>
                </div>
                <!-- end login-content -->
            </div>
            <!-- end right-container -->
        </div>
        <!-- end login -->
    </div>
    <!-- end page container -->
    </body>

@endsection
