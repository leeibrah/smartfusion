@if ($errors->any())
    <ul class="alert alert-danger">
@foreach ($errors->all() as $error)
    <p class="error">{{ $error }}</p>
@endforeach
    </ul>
    @endif
