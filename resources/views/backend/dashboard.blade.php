@extends('layouts.admin')

@section ('title', 'Dashboard')

@section('content')
    @role('Administrator')
    @include('backend.admincp')
    @endrole

    @role('User')
    @include('backend.usercp')
    @endrole

@endsection