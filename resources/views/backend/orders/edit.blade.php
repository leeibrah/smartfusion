@extends ('../../layouts.admin')

@section ('title', 'Services Management | Edit')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li><a href="{{url('admin/service')}}">Services Management</a></li>
            <li class="active">Edit: {!! $service->title !!}</li>
        </ol>
        <h1 class="page-header">Edit Service <small>Make Modifications to your Service</small></h1>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Edit: {!! $service->title !!}</h4>
            </div>
            <div class="panel-body">

    @include('../../errors/list')

    {!! Form::model($service, ['method' => 'PATCH', 'action' => ['ServiceController@update', $service->id]]) !!}

    @include('backend.services.form ', ['submitButtonText' => 'Update Service'])

    {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop