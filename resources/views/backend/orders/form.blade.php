<div class="box box-primary">
    <div class="box-body">

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, array('required','class'=>'form-control', 'placeholder'=>'Title'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('category', 'Category:') !!}
            {!! Form::text('category', null, array('required','class'=>'form-control', 'placeholder'=>'Choose Category'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, array('required','class'=>'form-control', 'placeholder'=>'Enter Description'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            {!! Form::text('image') !!}
        </div>

        <div class="form-group">
            {!! Form::label('delivery_time', 'Delivery Time:') !!}
            {!! Form::text('delivery_time', null, array('required','class'=>'form-control', 'placeholder'=>'Enter Delivery Time'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('features', 'Features:') !!}
            {!! Form::text('features', null, array('required','class'=>'form-control', 'placeholder'=>'Select Features'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('price', 'Price:') !!}
            {!! Form::text('price', null, array('required','class'=>'form-control', 'placeholder'=>'Enter Price'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('supplier', 'Supplier:') !!}
            {!! Form::text('supplier', null, array('required','class'=>'form-control', 'placeholder'=>'Select Supplier'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('external_id', 'Service:') !!}
            {!! Form::text('external_id', null, array('required','class'=>'form-control', 'placeholder'=>'Select Remote Service'))!!}
        </div>

        <div class="form-group">
            {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>