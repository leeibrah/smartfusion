@extends ('../../layouts.admin')

@section ('title', 'Orders Management')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">Orders Management</li>
        </ol>
        <h1 class="page-header">Manage Orders <small>Edit Orders</small></h1>

        <div class="pull-right" style="margin-bottom:10px">
            <a href="#" class="btn btn-primary btn">Accept Selected</a>
            <a href="#" class="btn btn-danger btn">Reject Selected</a>
            <a href="#" class="btn btn-primary btn">Accept All PENDING</a>
            <a href="#" class="btn btn-danger btn">Reject All PENDING</a>
        </div>
        <div class="clearfix"></div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Orders List</h4>
            </div>
            <div class="panel-body">
		<table id="order-table" class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>ID</th>
                    <th>IMEI</th>
                    <th>Service</th>
                    <th>Status</th>
					<th>Placed</th>
                    <th>Updated</th>
                    <th>Actions</th>
				</tr>
			</thead>
	</table>
	</div>
</div>
    </div>

@endsection

@push('scripts')
<script>
    var oTable = $('#order-table').DataTable({
		processing: true,
		serverSide: true,
        ajax: {
            url: "{{ url('admin/order/data/') }}",
            data: function (d) {
                d.imei = $('input[name=imei]').val();
                d.service = $('input[name=service]').val();
            }
        },
        dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
        "<'row'<'col-xs-12't>>"+
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        tableTools: { "sRowSelect": "os",
                     "aButtons": [ "select_all", "select_none", "print", { "sExtends": "collection", "sButtonText": "Save as", "aButtons": [ "csv", "xls", "pdf" ] } ] },
        columns: [
			{data: 'id', name: 'id'},
            {data: 'imei', name: 'imei'},
            {data: 'service', name: 'service'},
            {data: 'status', name: 'status'},
			{data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
                                 });
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });
</script>
@endpush