@extends ('../../layouts.admin')

@section ('title', 'Status Management')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">Order Status Management</li>
        </ol>
        <h1 class="page-header">Manage Order Status <small>Edit Order Status</small></h1>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Order Status List</h4>
            </div>
            <div class="panel-body">
		<table id="status-table" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Status</th>
                    <th>Actions</th>
				</tr>
			</thead>
	</table>
	</div>
             </div>
	       </div>

@endsection

@push('scripts')
<script>
$('#status-table').DataTable({
        processing: true,
		serverSide: true,
		ajax: "{{ URL::to('admin/status/data/') }}",

        columns: [
			{data: 'title', name: 'title'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
                                 });


</script>
@endpush