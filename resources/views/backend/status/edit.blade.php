@extends ('../../layouts.admin')

@section ('title', 'Status Management | Edit')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li><a href="{{url('admin/status')}}">Status Management</a></li>
            <li class="active">Edit: {!! $Status->title !!}</li>
        </ol>
        <h1 class="page-header">Edit Status <small>Make Modifications to your Statuses</small></h1>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Edit: {!! $status->title !!}</h4>
            </div>
            <div class="panel-body">

    @include('../../errors/list')

    {!! Form::model($status, ['method' => 'PATCH', 'action' => ['StatusController@update', $status->id]]) !!}

    @include('backend.status.form ', ['submitButtonText' => 'Update Order Status'])

    {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop