<div class="box box-primary">
    <div class="box-body">

       <span class="pull-right">
            {!! Form::select('active', array('on' => 'active', 'off' => 'Not active',), 
                Input::old('active', 1), 
                array('class'=>'btn btn-sm btn-primary', 'id'=>'active')) !!}
        </span>

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, array('class'=>'form-control', 'placeholder'=>'Title')) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Category:') !!}
            {!! Form::select('category', (['0' => 'Select a Category'] + $categories), $selectedCategoryKey, ['class' => 'form-control']) !!}
        </div>
       

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, array('required','class'=>'ckeditor'))!!}
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Image:') !!}
            {!! Form::file('image') !!}
        </div>

        <div class="form-group">
            {!! Form::label('delivery_time', 'Delivery Time:') !!}
            {!! Form::input('date','delivery_time', date('Y-m-d'), array('class'=>'form-control', 'placeholder'=>'Enter Delivery Time'))!!}
        </div>


        <div class="form-group">            
            {!! Form::label('feature_list', 'Features:') !!} <br>
            {!! Form::select('feature_list[]', $features, null, ['class'=>'form-control', 'multiple']) !!}
            <br />
        </div>

        <div class="form-group">
            {!! Form::label('price', 'Price Group:') !!}
            {!! Form::select('price', (['0' => 'Select a Price Group'] + $prices), $selectedPriceKey, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('supplier', 'Supplier API:') !!}
            {!! Form::select('supplier', (['0' => 'Select a Supplier'] + $suppliers), $selectedSupplierKey, ['class' => 'form-control']) !!}
        </div>

    
        <div class="form-group">
            {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>
<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>