<div class="form-group">
    {!! Form::label('description', 'Title:') !!}
    {!! Form::text('description', null, array('required','class'=>'form-control', 'placeholder'=>'Enter description for Feature'))!!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>