@extends ('../../layouts.admin')

@section ('title', 'Features Management')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">Features Management</li>
        </ol>
        <h1 class="page-header">Manage Features <small>Add/Remove/Edit Features</small></h1>

        <div class="pull-right" style="margin-bottom:10px">
            <a href="{{route('admin.feature.create')}}" class="btn btn-success btn"><i class="fa fa-plus"></i> New Feature</a>
        </div>
        <div class="clearfix"></div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Features List</h4>
            </div>
            <div class="panel-body">
		<table id="feature-table" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Title</th>
                    <th>Actions</th>
				</tr>
			</thead>
	</table>
	</div>
</div>
                </div>

@endsection

@push('scripts')
<script>
$(function() {
	$('#feature-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: "{{ URL::to('admin/feature/data/') }}",
        columns: [
			{data: 'id', name: 'id'},
			{data: 'description', name: 'description'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]        
        });
});
</script>
@endpush