 <div class="form-group">
            {!! Form::label('delivery_time', 'Features:') !!}<br>
            @foreach ($features as $feature)

                {!! Form::checkbox('feature[{{ $feature }}]', 'feature') !!}                
                {!! Form::label('checkbox', $feature) !!}
                <br>
            @endforeach

            


            {{--@foreach($features as $feature)
                {!! Form::label('features', 'Features:') !!} <br>
                {!! Form::checkbox('features') !!}

                {!! Form::checkbox('feature_a', 'feature_a') !!}
                {!! Form::label('checkbox', 'Feature A ') !!}
                <br />
                {!! Form::checkbox('feature_b', 'feature_b') !!}
                {!! Form::label('checkbox', 'Feature B  ') !!} 
                <br />
                {!! Form::checkbox('feature_c', 'feature_c') !!}
                {!! Form::label('checkbox', 'Feature C') !!}
                <br />
                {!! Form::checkbox('feature_d', 'feature_d') !!}
                {!! Form::label('checkbox', 'Feature D  ') !!} 
                <br />
                {!! Form::checkbox('feature_e', 'feature_e') !!}
                {!! Form::label('checkbox', 'Feature E  ') !!}
         
                <br />
            @endforeach--}}


            
        <div class = "form-group">
                {!! Form::label('Features') !!}
                <?php $features = App\Feature::all();
                $allowed = $features->lists('title');
                
                $value = false;?>
                @foreach($features as $feature)
                <?php $value = in_array($feature->id, $allowed); ?>
                <div class = "checkbox">
                    <label>
                        {!! Form::checkbox('allowed[]', $feature->id, $value) !!} {{ $feature->title }}
                    </label>
                </div>
                @endforeach
        </div>
        </div>




    $input = $request::all();
    $input['delivery_time'] =  Carbon::now();

    Service::create($input);

      // $service =  new Service;
      // $service->title = 'Service A';
      // $service->category = 1;
      // $service->description = 'Service A Description';
      // $service->delivery_time = Carbon::now();
      // $service->price = '$100';
      // // $service->supplier = 1;
      // // $service->user_id = 1;

      // $service->features()->feature_id = 1;
      // $service->features()->service_id = 1;
      // $service->features()->created_at = DateTime::createFromFormat('y', '13');
      // $service->features()->updated_at = DateTime::createFromFormat('y', '13');
      // $service->save();


      // $service = DB::table('services');
      // $service->created_at = Carbon::now();
      // dd($service);
    // $input = Input::all();
    //   $service = Service::create($input);

      // $service->features()->attach($request->input('features'));

      // return redirect('admin/service');



           $service =  new Service;
      $service->title = 'Service A';
      $service->category = 1;
      $service->description = 'Service A Description';
      $service->delivery_time = 'time';
      $service->price = '$100';
      $service->supplier = 1;
      $service->user_id = 1;

      $service->features()->feature_id = 1;
      $service->features()->service_id = 1;
      $service->features()->created_at = DateTime::createFromFormat('y', '13');
      $service->features()->updated_at = Carbon::now();
      $service->save();



      $input = Request::all();
      $input['delivery_time'] = Carbon::now();

      Service::create($input);

      return redirect('admin/service');


            $service = Auth::user()->services()->create($request->all());

      $service->features()->attach($request->input('features'));

      return redirect('admin/service');

      $service = Service::create($request->all());


      <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturesTable extends Migration {

  public function up()
  {
    Schema::create('features', function(Blueprint $table) {
      $table->increments('id');
      $table->string('description');  
      $table->timestamps();
    });

    Schema::create('feature_service', function(Blueprint $table){

      $table->integer('feature_id')->unsigned()->index();
      $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');

      $table->integer('service_id')->unsigned()->index();
      $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

      // $table->timestamps()->nullable();

    });
  }

  public function down()
  {
    Schema::drop('features');
    Schema::drop('feature_service');
    // Schema::drop('features');
  }
}