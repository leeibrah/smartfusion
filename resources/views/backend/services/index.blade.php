@extends ('../../layouts.admin')

@section ('title', 'Services Management')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">Services Management</li>
        </ol>
        <h1 class="page-header">Manage Services <small>Add/Remove/Edit Services</small></h1>

        <div class="pull-right" style="margin-bottom:10px">
            <a href="{{route('admin.service.create')}}" class="btn btn-success btn"><i class="fa fa-plus"></i> New Service</a>  
            <a href="#" class="btn btn-primary btn" onclick="activate()">Activate Selected</a> 
            <a href="#" class="btn btn-danger btn" onclick="GetCellValues()">Deactivate Selected</a>

            <input type="button" onclick="activate()" value="Click">
        </div>
        <div class="clearfix"></div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Services List</h4>
            </div>
            <div class="panel-body">
        		<table id="service-table" class="table table-striped table-hover table-bordered" data-click-to-select="true">
            			<thead>
            				<tr>
            					<th>ID</th>
                                <th>Service</th>
                                <th>Description</th>
            					<th>Actions</th>
            				</tr>
            			</thead>
            	</table>

                <!-- <table data-url="data1.json" data-height="299" data-click-to-select="true">
                    <thead>
                        <tr>
                            <th data-field="state" data-checkbox="true"></th>
                            <th data-field="id" data-align="right">Item ID</th>
                            <th data-field="name" data-align="center">Item Name</th>
                            <th data-field="price" data-align="">Item Price</th>
                        </tr>
                    </thead>
                </table> -->

        	</div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
$(function() {
    var category = 'category';
    var price = price;
    $('#service-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: "{{ URL::to('admin/service/data/') }}",
        dom: 'T<"clear">lfrtip',
        tableTools: { "sRowSelect": "os",
                     "aButtons": [ "select_all", "select_none", "print",
                         { "sExtends": "collection", "sButtonText": "Save as", "aButtons": [ "csv", "xls", "pdf" ] } ] },
        columns: [
			{data: 'id', name: 'id'},
			{data: 'title', name: 'title'},
            {data: 'description', name: 'description' },
			// {data: 'price, name: 'price'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
    });

    console.log('id');
});

function activate(){
    console.log('activated');
}

function GetCellValues() {
    var table = $('#service-table').DataTable( {
    columns: [
        { name: 'id' },
        { title: 'description'}
        ]
    } );
     
    // Get salary column data
    var data = table.column( 'id:name' ).data();
    console.log(data[0]);

}


</script>
@endpush

