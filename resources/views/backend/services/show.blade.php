@extends ('../../layouts.admin')

@section ('title', 'Services Management Show')

@section('content')
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-right">
			<li><a href="javascript:;">Home</a></li>
			<li><a href="javascript:;">Page Options</a></li>
			<li class="active">Service Management Show</li>
		</ol>
		<!-- end breadcrumb -->
		<!-- begin page-header -->
		<h1 class="page-header">Service Management Show</h1>
		<!-- end page-header -->
		
		<div class="panel panel-inverse">
		    <div class="panel-heading">
		        <div class="panel-heading-btn">
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand" data-original-title="" title=""><i class="fa fa-expand"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload" data-original-title="" title=""><i class="fa fa-repeat"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title=""><i class="fa fa-minus"></i></a>
		            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		        </div>
		        <h4 class="panel-title">{!! $service->title !!}</h4>
		    </div>
		    <div class="panel-body">
		        <div class="note note-success">
					<h4>Image: <span>Image heres</span></h4>
					<h4>Category: <span>{!! $service->category !!}</span></h4>
					<h4>Description: <span>{!! $service->description !!}</span></h4>
					<h4>Features: <span>{!! $service->features !!}</span></h4>
					<h4>Delivery Time: <span>{!! $service->delivery_time !!}</span></h4>
					<h4>Price: <span>{!! $service->price !!}</span></h4>
					<h4>Supplier: <span>{!! $service->supplier !!}</span></h4>
				</div>
		    </div>
		</div>
	</div>

@endsection