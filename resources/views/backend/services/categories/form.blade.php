<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, array('required','class'=>'form-control', 'placeholder'=>'Title'))!!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>