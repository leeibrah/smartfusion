@extends ('../../layouts.admin')

@section ('title', 'Categories Management')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">Categories Management</li>
        </ol>
        <h1 class="page-header">Manage Categories <small>Add/Remove/Edit Categories</small></h1>

        <div class="pull-right" style="margin-bottom:10px">
            <a href="{{route('admin.category.create')}}" class="btn btn-success btn"><i class="fa fa-plus"></i> New Category</a>
        </div>
        <div class="clearfix"></div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Categories List</h4>
            </div>
            <div class="panel-body">
		<table id="category-table" class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Category Name</th>
                    <th>Actions</th>
				</tr>
			</thead>
	</table>
	</div>
             </div>
	       </div>

@endsection

@push('scripts')
<script>
$('#category-table').DataTable({
        processing: true,
		serverSide: true,
		ajax: "{{ URL::to('admin/category/data/') }}",

        columns: [
			{data: 'id', name: 'id'},
			{data: 'title', name: 'title'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		]
                                 });


        

</script>
@endpush