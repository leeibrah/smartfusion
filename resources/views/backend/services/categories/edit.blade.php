@extends ('../../layouts.admin')

@section ('title', 'Features Management | Edit')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li><a href="{{url('admin/category')}}">Categories Management</a></li>
            <li class="active">Edit: {!! $category->title !!}</li>
        </ol>
        <h1 class="page-header">Edit Category <small>Make Modifications to your Categories</small></h1>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">Edit: {!! $category->title !!}</h4>
            </div>
            <div class="panel-body">

    @include('../../errors/list')

    {!! Form::model($category, ['method' => 'PATCH', 'action' => ['CategoryController@update', $category->id]]) !!}

    @include('backend.services.categories.form ', ['submitButtonText' => 'Update Category'])

    {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop