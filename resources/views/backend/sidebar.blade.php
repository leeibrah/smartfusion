<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                    <a href="javascript:;"><img src="{{url('assets/img/user-11.jpg')}}" alt="" /></a>
                </div>
                <div class="info">
                    Sean Ngu
                    <small>Front end developer</small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->

        <ul class="nav">
            <li class="nav-header">Navigation</li>

            <li>
                <a href="{{url('/dashboard')}}"><i class="fa fa-laptop"></i> <span>Dashboard</span></a>
            </li>

            @role('Administrator')
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>Users Management</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{route('access.users.index')}}">Users</a></li>
                    <li><a href="{{route('access.roles.index')}}">Roles</a></li>
                    <li><a href="{{route('access.roles.permissions.index')}}">Permisions</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>Services Management</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{url('admin/service')}}">Services</a></li>
                    <li><a href="{{url('admin/category')}}">Categories</a></li>
                    <li><a href="{{url('admin/feature')}}">Features</a></li>
                    <li><a href="{{url('admin/price')}}">Prices</a></li>
                </ul>
            </li>
            <li>
                <a href="{{url('admin/order')}}"><i class="fa fa-laptop"></i> <span>Orders Management</span></a>
            </li>
            <li>
                <a href="{{url('admin/supplier')}}"><i class="fa fa-laptop"></i> <span>Suppliers Management</span></a>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>Settings</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{url('admin/service')}}">General Settings</a></li>
                    <li><a href="{{url('admin/status')}}">Statuses</a></li>
                    <li><a href="{{url('admin/feature')}}">Features</a></li>
                    <li><a href="{{url('admin/price')}}">Prices</a></li>
                </ul>
            </li>
            <li class="nav-header">Reports</li>
            @endrole

        @role('User')
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>Orders</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="{{url('user/order/create')}}">Place Order</a></li>
                    <li><a href="#">Orders History</a></li>
                   </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-laptop"></i> <span>Services</span></a>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>Credits</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="#">Add Credits</a></li>
                    <li><a href="#">Credits History</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i>
                    <b class="caret pull-right"></b>
                    <span>My Account</span>
                </a>
                <ul class="sub-menu">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Statement</a></li>
                </ul>
            </li>
            <li class="nav-header">Reports</li>
        @endrole
                <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
         <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
