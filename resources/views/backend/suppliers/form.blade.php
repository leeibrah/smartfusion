<div class="box box-primary">
     <div class="box-body">

<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, array('required','class'=>'form-control', 'placeholder'=>'Title'))!!}
</div>

<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('username', null, array('required','class'=>'form-control', 'placeholder'=>'Enter your username for remote API'))!!}
</div>

<div class="form-group">
    {!! Form::label('key', 'Key:') !!}
    {!! Form::text('key', null, array('required','class'=>'form-control', 'placeholder'=>'Enter API KEY'))!!}
</div>

<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, array('required','class'=>'form-control', 'placeholder'=>'Enter remote API url'))!!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>
    </div>
</div>