@extends ('../../layouts.admin')

@section ('title', 'Suppliers Management | Add')

@section('content')

    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li><a href="{{url('admin/supplier')}}">Suppliers Management</a></li>
            <li class="active">Add Supplier API</li>
        </ol>
        <h1 class="page-header">Add Supplier API <small>Add new Supplier API</small></h1>


        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

                </div>
                <h4 class="panel-title">API Details</h4>
            </div>
            <div class="panel-body">

    @include('../../errors/list')

    {!! Form::open(['action' => 'SupplierController@store']) !!}

    @include('backend.suppliers.form', ['submitButtonText' => 'Add API'])

    {!! Form::close() !!}
</div>
        </div>
        </div>
@stop