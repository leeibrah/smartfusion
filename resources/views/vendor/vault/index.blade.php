@extends ('layouts.admin')

@section ('title', 'User Management')


@section('content')
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li><a href="{{url('/dashboard')}}">Dashboard</a></li>
            <li class="active">User Management</li>
        </ol>
        <h1 class="page-header">Active Users <small>Manage your active users</small></h1>

            <div class="pull-right" style="margin-bottom:10px">
                <a href="{{route('access.users.create')}}" class="btn btn-success btn"><i class="fa fa-plus"></i> New User</a> <a href="{{route('access.users.deactivated')}}" class="btn btn-warning btn"><i class="fa fa-user"></i> Deactivated Users</a> <a href="{{route('access.users.deleted')}}" class="btn btn-danger btn"><i class="fa fa-times"></i> Deleted Users</a>
            </div>
            <div class="clearfix"></div>

        <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>

            </div>
            <h4 class="panel-title">Panel Title here</h4>
        </div>
        <div class="panel-body">

			<table class="table table-striped table-bordered table-hover">
				<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>E-mail</th>
					<th>Roles</th>
					<th>Other Permissions</th>
					<th class="visible-lg">Created</th>
					<th class="visible-lg">Last Updated</th>
					<th>Actions</th>
				</tr>
				</thead>
				<tbody>
					@foreach ($users as $user)
						<tr>
							<td>{!! $user->id !!}</td>
							<td>{!! $user->name !!}</td>
							<td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
							<td>
								@if ($user->roles()->count() > 0)
									@foreach ($user->roles as $role)
										{!! $role->name !!}<br/>
									@endforeach
								@else
									None
								@endif
							</td>
							<td>
                                @if ($user->permissions()->count() > 0)
                                    @foreach ($user->permissions as $perm)
                                        {!! $perm->display_name !!}<br/>
                                    @endforeach
                                @else
                                    None
                                @endif
                            </td>
							<td class="visible-lg">{!! $user->created_at->diffForHumans() !!}</td>
							<td class="visible-lg">{!! $user->updated_at->diffForHumans() !!}</td>
							<td>{!! $user->action_buttons !!}</td>
						</tr>
					@endforeach
				</tbody>
			</table>

			<div class="widget-foot">
				<div class="pull-left">
					{{ $users->total() }} user(s) total
				</div>

				<div class="pull-right">
					{{ $users->render() }}
				</div>

				<div class="clearfix"></div>
			</div><!--widget foot-->


        </div>
    </div>
    </div>
@stop